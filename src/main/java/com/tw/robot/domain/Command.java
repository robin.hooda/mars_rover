package com.tw.robot.domain;

public abstract class Command {

    public abstract void makeMovement(Robot robot) throws OutOfGridException;

}
