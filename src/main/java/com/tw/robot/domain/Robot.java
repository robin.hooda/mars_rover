package com.tw.robot.domain;

import com.tw.robot.console.InvalidLocationException;

import java.util.Objects;

public class Robot {
    private final Position position;
    private final Location location;
    private final Grid grid;
    private Command command;

    public Robot(Position position, Location location, Grid grid) {
        this.position = position;
        this.location = location;
        this.grid = grid;
    }

    public Robot left() {

        return new Robot(this.position, this.location.left, grid);
    }

    public Robot right() {
        return new Robot(this.position, this.location.right, grid);
    }

    public Robot move() throws OutOfGridException {
        Position expectedPosition = this.location.movingFactor.plus(this.position);

        if (expectedPosition.isIn(this.grid)) {
            return new Robot(expectedPosition, this.location, grid);
        }
        throw new OutOfGridException();
    }

    public Position positionOfRobot() {
        return position;
    }

    public Location locationOfRobot() {
        return location;
    }

    public static Location getLocation(String location) throws InvalidLocationException {
        if (location.equals("N")) {
            return Location.N;
        } else if (location.equals("E")) {
            return Location.E;
        } else if (location.equals("S")) {
            return Location.S;
        } else if (location.equals("W")) {
            return Location.W;
        } else {
            throw new InvalidLocationException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return Objects.equals(position, robot.position) &&
                Objects.equals(location, robot.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, location);
    }

    public String getFinalDirection(Robot robot) {
        return null;
    }


    public final static class Location {
        public static final Location N = new Location(new Position(0, 1));
        public static final Location S = new Location(new Position(0, -1));
        public static final Location E = new Location(new Position(1, 0));
        public static final Location W = new Location(new Position(-1, 0));

        static {
            N.right = E;
            N.left = W;
        }

        static {
            E.right = S;
            E.left = N;

        }

        static {
            S.right = W;
            S.left = E;
        }

        static {
            W.right = N;
            W.left = S;
        }

        final Position movingFactor;
        Location left;
        Location right;

        Location(Position movingFactor) {
            this.movingFactor = movingFactor;
        }

    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return this.command;
    }

    public void commandAction() throws OutOfGridException {
        command.makeMovement(this);

    }
}
