package com.tw.robot.domain;

import java.util.Objects;

public class Position {
    private final int xCoordinate;
    private final int yCoordinate;

    public Position(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    Position plus(Position position) {
        return new Position(
                this.xCoordinate + position.xCoordinate,
                this.yCoordinate + position.yCoordinate);
    }

    boolean isIn(Grid grid) {
        return grid.isInsideGrid(xCoordinate, yCoordinate);
    }

    @Override
    public String toString() {
        return  xCoordinate + " "+ yCoordinate ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return xCoordinate == position.xCoordinate &&
                yCoordinate == position.yCoordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xCoordinate, yCoordinate);
    }
}
