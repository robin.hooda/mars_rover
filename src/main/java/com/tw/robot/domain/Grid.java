package com.tw.robot.domain;

import java.util.Objects;

public class Grid {
    private final int size;

    public Grid(int size) {
        this.size = size;
    }

    public boolean isInsideGrid(int xCoordinate, int yCoordinate) {
        return xCoordinate >= 0 && xCoordinate <= size && yCoordinate >= 0 && yCoordinate <= size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Grid grid = (Grid) o;
        return size == grid.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size);
    }
}
