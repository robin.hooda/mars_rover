package com.tw.robot.domain;

public class LeftCommand extends Command {

    @Override
    public void makeMovement(Robot robot) {
        robot.left();
    }
}
