package com.tw.robot.domain;

public class MoveCommand extends Command {

    @Override
    public void makeMovement(Robot robot) throws OutOfGridException {
        robot.move();
    }
}
