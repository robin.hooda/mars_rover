package com.tw.robot.domain;

public class RightCommand extends Command {

    @Override
    public void makeMovement(Robot robot) throws OutOfGridException {
        robot.right();
    }
}
