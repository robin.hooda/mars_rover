package com.tw.robot.domain;

public class FinalDirection {
    public static String getFinalDirection(Robot robot) {
        String finalDirection = "";
        Robot.Location locationOfRobot = robot.locationOfRobot();
        if (Robot.Location.N.equals(locationOfRobot)) {
            finalDirection = "N";
        } else if (Robot.Location.S.equals(locationOfRobot)) {
            finalDirection = "S";
        } else if (Robot.Location.E.equals(locationOfRobot)) {
            finalDirection = "E";
        } else if (Robot.Location.W.equals(locationOfRobot)) {
            finalDirection = "W";
        }
        return finalDirection;
    }

}
