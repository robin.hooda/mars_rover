package com.tw.robot.console;

import com.tw.robot.domain.*;

import java.util.Scanner;

import static com.tw.robot.domain.Robot.getLocation;

public class RobotApplication {

    public static void main(String[] args) throws Exception, OutOfGridException, InvalidLocationException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the grid size (e.g. Enter 4 for 4 * 4 grid)");
        int sizeOfGrid = scanner.nextInt();

        System.out.println("\nEnter the x co-ordinate, y co-ordinate and the direction of the Robot (e.g. 2 5 N )");
        int xCoordinate = scanner.nextInt();
        int yCoordinate = scanner.nextInt();
        String location = scanner.next();
        Grid grid = new Grid(sizeOfGrid);
        Position position = new Position(xCoordinate, yCoordinate);
        Robot robot = new Robot(position, getLocation(location), grid);

        while (true) {
            System.out.println("Enter the command (e.g. L - left, R - Right, M - Movement ): ");
            String command = scanner.next();
            switch (command){
                case "L":   robot.setCommand(new LeftCommand());
                            robot.commandAction();
                            break;
                case "R":   robot.setCommand(new RightCommand());
                            robot.commandAction();
                            break;
                case "M":   robot.setCommand(new MoveCommand());
                            robot.commandAction();
                            break;
                default:    System.out.println("Invalid Input !");
            }
            String finalDirection = FinalDirection.getFinalDirection(robot);
            System.out.println("Do you want to add more command for Robot- Press n for no ?");
            String answer = scanner.next();

            if (answer.equals("no")) {
                System.out.println("Robot new position " + position + " \nDirection :" + finalDirection);
                break;
            }
        }
    }
}
